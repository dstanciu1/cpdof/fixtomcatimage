FROM tomcat:latest

RUN [ ! "$(ls -A /usr/local/tomcat/webapps)" ] && mv /usr/local/tomcat/webapps.dist/* /usr/local/tomcat/webapps

ADD tomcat-users.xml /usr/local/tomcat/conf/
ADD context.xml /usr/local/tomcat/webapps/manager/META-INF/
